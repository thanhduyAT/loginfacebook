//
//  StreamURL.swift
//  my login facebook
//
//  Created by Thanh Duy AT on 4/15/21.
//

import Foundation

final class StreamURL: Codable {
    var id: String
    var streamURL: String
    var secureStreamURL: String
    
    init(id: String, streamURL: String, secureStreamURL: String) {
        self.id = id
        self.streamURL = streamURL
        self.secureStreamURL = secureStreamURL
    }
    
    private enum CodingKeys : String, CodingKey {
        case id, streamURL = "stream_url", secureStreamURL = "secure_stream_url"
    }
}
