//
//  APIRequest.swift
//  my login facebook
//
//  Created by Thanh Duy AT on 4/15/21.
//

import Foundation

enum APIError: Error {
    case responseProblem
    case decodingProblem
    case encodingProblem
}

struct APIRequest {
    let resourceURL: URL
    
    init(enpoint: String) {
        let resourceString = "https://graph.facebook.com/v3.3/me/live_videos?status=LIVE_NOW&access_token=\(enpoint)"
        guard  let resourceURL = URL(string: resourceString) else {fatalError()}
        
        self.resourceURL = resourceURL
    }
    
    func save(urlToSave: String, completion: @escaping(Result<StreamURL, APIError>) -> Void) {
        let postToken = [
            "target_token": "\(urlToSave)"
        ]
        
        do {
            var urlRequest = URLRequest(url: resourceURL)
            urlRequest.httpMethod = "POST"
            urlRequest.addValue("application/json", forHTTPHeaderField: "Content-Type")
            urlRequest.httpBody = try? JSONEncoder().encode(postToken)
            
            let dataTask = URLSession.shared.dataTask(with: urlRequest) { data, response, _ in
                guard let httpRespond = response as? HTTPURLResponse, httpRespond.statusCode == 200, let jsonData = data  else {
                    completion(.failure(.responseProblem))
                    return
                }
                
                do {
                    let urlData = try JSONDecoder().decode(StreamURL.self, from: jsonData)
                    completion(.success(urlData))
                } catch {
                    completion(.failure(.decodingProblem))
                }
            }
            dataTask.resume()
        } catch {
            completion(.failure(.encodingProblem))
        }
    }
}
