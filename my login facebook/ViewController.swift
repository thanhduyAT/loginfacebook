//
//  ViewController.swift
//  my login facebook
//
//  Created by Thanh Duy AT on 4/14/21.
//

import UIKit
import FBSDKLoginKit


class ViewController: UIViewController {

    override func viewDidLoad() {
        super.viewDidLoad()
        if let token = AccessToken.current, !token.isExpired {
            let token = token.tokenString
            
//            let url = URL(string: "https://graph.facebook.com/v3.3/me/live_videos?status=LIVE_NOW&access_token=\(token)")!
//            var request = URLRequest(url: url)
//            request.setValue("authToken", forHTTPHeaderField: "Content-Type")
//            request.httpMethod = "POST"
//            let parameters: [String: Any] = [
//                "target_token": "\(token)"
//            ]
//            request.httpBody = try? JSONSerialization.data(withJSONObject: parameters, options:  [])
//
//            let task = URLSession.shared.dataTask(with: request) { data, response, error in
//                guard let data = data,
//                    let response = response as? HTTPURLResponse,
//                    error == nil else {                                              // check for fundamental networking error
//                    print("error", error ?? "Unknown error")
//                    return
//                }
//
//                guard (200 ... 299) ~= response.statusCode else {                    // check for http errors
//                    print("statusCode should be 2xx, but is \(response.statusCode)")
//                    print("response = \(response)")
//                    return
//                }
//
//                let responseString = String(data: data, encoding: .utf8)
//                print("responseString = \(responseString)")
//            }
//
//            task.resume()
            let postRequest = APIRequest(enpoint: token)

            postRequest.save(urlToSave: token) { (result) in
                switch result {
                case .success(let message):
                    print("\(message)")
                case .failure(let message):
                    print("\(message)")
                }
            }
            
            let request = FBSDKLoginKit.GraphRequest(graphPath: "me", parameters: ["fields": "name, email, picture"], tokenString: token, version: nil, httpMethod: .get)
            request.start { (connection, result, error) in
                print("\(result)")
            }

        } else {
            let loginButton = FBLoginButton()
            loginButton.center = view.center
            loginButton.delegate = self
            loginButton.permissions = ["public_profile", "email","public_video"]
            view.addSubview(loginButton)
        }
    }
 


}
extension ViewController: LoginButtonDelegate {
    func loginButton(_ loginButton: FBLoginButton, didCompleteWith result: LoginManagerLoginResult?, error: Error?) {
        let token = result?.token?.tokenString
        
//        let url = URL(string: "https://graph.facebook.com/v3.3/me/live_videos?status=LIVE_NOW&access_token=\(token)")!
//        var request = URLRequest(url: url)
//        request.setValue("authToken", forHTTPHeaderField: "Content-Type")
//        request.httpMethod = "POST"
//        let parameters: [String: Any] = [
//            "target_token": "\(token)"
//        ]
//        request.httpBody = try? JSONSerialization.data(withJSONObject: parameters, options:  [])
//
//        let task = URLSession.shared.dataTask(with: request) { data, response, error in
//            guard let data = data,
//                let response = response as? HTTPURLResponse,
//                error == nil else {                                              // check for fundamental networking error
//                print("error", error ?? "Unknown error")
//                return
//            }
//
//            guard (200 ... 299) ~= response.statusCode else {                    // check for http errors
//                print("statusCode should be 2xx, but is \(response.statusCode)")
//                print("response = \(response)")
//                return
//            }
//
//            let responseString = String(data: data, encoding: .utf8)
//            print("responseString = \(responseString)")
//        }
//
//        task.resume()
        let postRequest = APIRequest(enpoint: token ?? "")

        postRequest.save(urlToSave: token ?? "") { (result) in
            switch result {
            case .success(let message):
                print("\(message)")
            case .failure(let message):
                print("\(message)")
            }
        }
    }
    
    func loginButtonDidLogOut(_ loginButton: FBLoginButton) {
        
    }
}

